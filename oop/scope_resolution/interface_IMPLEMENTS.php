<?php
// Method Interface Harus di set Sebagai Public


interface mouse{
   const JENIS = "Laser Mouse";
   public function klik_kanan();
   public function klik_kiri();
}
  
class laptop implements mouse{
   public function klik_kanan(){
     return "Klik Kanan oke...";
   }
   public function klik_kiri(){
     return "Klik Kiri...".mouse::JENIS;
   }

   
}
 
$laptop_baru = new laptop();
echo $laptop_baru->klik_kanan();
// Klik Kanan oke...


// **************************************************************************************
// Interface bisa di Turunkan (Inherit)
interface mouse{
   public function klik_kanan();
   public function klik_kiri();
}
  
interface mouse_gaming extends mouse{
   public function ubah_dpi();
}
  
class laptop implements mouse_gaming{
   public function klik_kanan(){
     return "Klik Kanan...";
   }
  
   public function klik_kiri(){
     return "Klik Kiri...";
   }
  
   public function ubah_dpi(){
     return "Ubah settingan DPI mouse";
   }

   
}
  
$laptop_baru = new laptop();
echo $laptop_baru->ubah_dpi();
// Ubah settingan DPI mouse

// *************************************************************************************************
// Sebuah Class Bisa Menggunakan Banyak Interface
interface mouse{
   public function klik_kanan();
   public function klik_kiri();
}
  
interface keyboard{
   public function tekan_enter();
}
  
class laptop implements mouse, keyboard{
   public function klik_kanan(){
     return "Klik Kanan...";
   }
  
   public function klik_kiri(){
     return "Klik Kiri...";
   }
  
   public function tekan_enter(){
     return "Tekan Tombol Enter...";
   }
}
  
$laptop_baru = new laptop();
echo $laptop_baru->tekan_enter();
// Tekan Tombol Enter...