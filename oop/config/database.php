<?php 

return [
    'mysql'     => [
        'name'      => 'mysql',
        'host'      => 'localhost',
        'username'  => 'root',
        'password'  => '',
        'port'      => '3306',
        'dbname'    => 'rest_ci'
    ]
];