<?php

<?php
// buat abstract class
abstract class komputer{
   // buat abstract method
   abstract public function lihat_spec();
}
  
class laptop extends komputer{
   public function beli_laptop(){
     return "Beli Laptop...";
   }
}
  
// buat objek dari class laptop
$laptop_baru = new laptop();
  
// Fatal error: Class laptop contains 1 abstract method
// and must therefore be declared abstract or implement
// the remaining methods (komputer::lihat_spec)
?>




// buat abstract class
abstract class komputer{
   // buat abstract method
   abstract public function lihat_spec();
}
  
class laptop extends komputer{
  
// implementasi abstract method
   public function lihat_spec(){
     return "Lihat Spec Laptop...";
   }
  
// method 'biasa'  
   public function beli_laptop(){
     return "Beli Laptop...";
   }
}
  
// buat objek dari class laptop
$laptop_baru = new laptop();
echo $laptop_baru->lihat_spec();
// Lihat Spec Laptop...
  
echo "<br />";
  
echo $laptop_baru->beli_laptop();
// Beli Laptop...





// ==========================================================================================
// buat abstract class
abstract class komputer{
   // buat abstract method
   abstract public function lihat_spec();
   abstract public function lihat_processor();
   abstract public function lihat_harddisk();
   abstract public function lihat_pemilik();
}
  
class laptop extends komputer{
       // ..semua abstarct komputer yg jika di extend harus di ada disini lihat_speclihat_processor();lihat_harddisk();lihat_pemilik();
}
  
class pc extends komputer{
   // ..semua abstarct komputer yg jika di extend harus di ada disini lihat_speclihat_processor();lihat_harddisk();lihat_pemilik();
}
  
class netbook extends komputer{
   // ..semua abstarct komputer yg jika di extend harus di ada disini lihat_speclihat_processor();lihat_harddisk();lihat_pemilik();
}