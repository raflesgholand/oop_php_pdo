<?php 
if(isset($_POST['save_mahasiswa'])){
    require __DIR__.'/../../models/M_mahasiswa.php';
    $mhs = new M_mahasiswa;
    $mhs->insert(
        [
        'nim' => $_POST['nim'], 
        'nama' => $_POST['nama'], 
        'jenis_kelamin' => $_POST['jenis_kelamin'], 
        'alamat' => $_POST['alamat']
        ]
    );
}
header('Location: index.php');
?>