/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.31-MariaDB : Database - rest_ci
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rest_ci` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rest_ci`;

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `jurusan` */

DROP TABLE IF EXISTS `jurusan`;

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `keys` */

DROP TABLE IF EXISTS `keys`;

CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `limits` */

DROP TABLE IF EXISTS `limits`;

CREATE TABLE `limits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `nim` int(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `id_jurusan` int(11) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_shipment_cloud` */

DROP TABLE IF EXISTS `t_shipment_cloud`;

CREATE TABLE `t_shipment_cloud` (
  `noid` bigint(20) NOT NULL AUTO_INCREMENT,
  `Sppb` varchar(20) DEFAULT '''',
  `tglsppb` varchar(10) DEFAULT '''''',
  `nopibk` varchar(10) DEFAULT '''' COMMENT 'BC nopibk',
  `tglpibk` varchar(10) DEFAULT '''''' COMMENT 'BC tglpibk',
  `kntr` varchar(30) DEFAULT '''''' COMMENT 'BC kntr',
  `kdkntr` varchar(6) DEFAULT '''''' COMMENT 'BC kdkntr',
  `bc11` varchar(10) DEFAULT '''''' COMMENT 'BC bc11',
  `tglbc` varchar(10) DEFAULT '''''' COMMENT 'BC tglbc',
  `nopos` varchar(4) DEFAULT '0' COMMENT 'BC pos',
  `subpos` varchar(8) DEFAULT '0' COMMENT 'BC subpos',
  `invoice` varchar(15) DEFAULT '''' COMMENT 'BC invoice',
  `tglInvo` varchar(10) DEFAULT '''''' COMMENT 'BC tglinvo',
  `mawb` varchar(12) DEFAULT '''''' COMMENT 'BC mawb',
  `hawb` varchar(25) DEFAULT '''' COMMENT 'BC hawb',
  `connote` varchar(25) DEFAULT '',
  `tglawb` varchar(10) DEFAULT '''''' COMMENT 'BC tglawb',
  `negaraasal` varchar(20) DEFAULT '''''' COMMENT 'BC negaraasal',
  `noutk` varchar(3) DEFAULT '''''' COMMENT 'BC noutk',
  `nmkirim` varchar(50) DEFAULT '''''' COMMENT 'BC nmkirim',
  `almkirim` varchar(250) DEFAULT '''''' COMMENT 'BC almkirim',
  `kdkirim` varchar(5) DEFAULT '' COMMENT 'BC kdkirim',
  `idterima` varchar(25) DEFAULT '''''',
  `No_idterima` varchar(20) DEFAULT '''''',
  `shipper_account` varchar(20) DEFAULT '''''',
  `shipper_name` varchar(50) DEFAULT '',
  `shipper_company` varchar(100) DEFAULT '',
  `shipper_address1` varchar(50) DEFAULT '',
  `shipper_address2` varchar(50) DEFAULT '',
  `shipper_city` varchar(30) DEFAULT '',
  `shipper_state` varchar(30) DEFAULT '',
  `shipper_country` varchar(3) DEFAULT '',
  `shipper_zip` varchar(10) DEFAULT '',
  `shipper_phone` varchar(20) DEFAULT '',
  `consignee_account` varchar(20) DEFAULT '' COMMENT 'BC idterima',
  `consignee_name` varchar(50) DEFAULT '' COMMENT 'BC nmterima',
  `consignee_pic` varchar(35) DEFAULT '',
  `consignee_address` varchar(250) DEFAULT '' COMMENT 'BC almterima',
  `consignee_city` varchar(25) DEFAULT '',
  `consignee_zip` varchar(6) DEFAULT '',
  `consignee_country` varchar(3) DEFAULT '',
  `consignee_phone` varchar(20) DEFAULT '',
  `consignee_NPWP` varchar(25) NOT NULL DEFAULT '',
  `consignee_email` varchar(75) DEFAULT NULL,
  `Ident_kode_broker` varchar(20) DEFAULT '' COMMENT 'BC idtahu',
  `NO_ident_broker` varchar(20) DEFAULT '' COMMENT 'BC noidtahu',
  `broker_name` varchar(30) DEFAULT '' COMMENT 'BC nmtahu',
  `broker_address` varchar(200) DEFAULT '',
  `broker_city` varchar(30) DEFAULT '',
  `broker_country` varchar(20) DEFAULT NULL,
  `broker_phone` varchar(12) DEFAULT '',
  `broker_ijin` varchar(40) DEFAULT '' COMMENT 'BC noizin',
  `broker_ijin_date` varchar(10) DEFAULT '' COMMENT 'BC tglizin',
  `shipmentCode` char(1) DEFAULT '' COMMENT 'BC angkut',
  `FlightName` varchar(30) DEFAULT '' COMMENT 'BC nmsarana ',
  `FlightCode` varchar(2) DEFAULT '' COMMENT 'BC  voy',
  `flightno` varchar(6) DEFAULT '',
  `flight_date` varchar(10) DEFAULT '',
  `ship_date` varchar(10) DEFAULT '',
  `Origin` varchar(5) DEFAULT '' COMMENT 'muat',
  `Destination` varchar(5) DEFAULT '' COMMENT 'bongkar',
  `currency_valuta` varchar(5) DEFAULT '' COMMENT 'valuta',
  `FOB` float NOT NULL DEFAULT '0' COMMENT 'FOB',
  `FOB_original` float NOT NULL DEFAULT '0',
  `FreightCost` float NOT NULL DEFAULT '0' COMMENT 'freight',
  `insurance` float NOT NULL DEFAULT '0' COMMENT 'asuransi',
  `CF` float NOT NULL DEFAULT '0',
  `CIF` float NOT NULL DEFAULT '0' COMMENT 'cif',
  `kindofGood` varchar(150) DEFAULT '' COMMENT 'urbar',
  `Package` int(11) NOT NULL DEFAULT '0' COMMENT 'kemas',
  `Weight` float NOT NULL DEFAULT '0' COMMENT 'berat',
  `weight_original` float NOT NULL DEFAULT '0',
  `hsCode` varchar(20) DEFAULT '' COMMENT 'hs',
  `NonDoc` char(1) NOT NULL DEFAULT 'Y',
  `ndpbm` decimal(10,0) DEFAULT '0' COMMENT 'ndpbm',
  `customsvalues` decimal(10,0) NOT NULL DEFAULT '0' COMMENT 'nilaipabean',
  `bm_persen` double NOT NULL DEFAULT '0' COMMENT 'BC bmper',
  `bm_value` double NOT NULL DEFAULT '0' COMMENT 'BC bm',
  `cukai_persen` double NOT NULL DEFAULT '0' COMMENT 'BC cukaiper',
  `cukai_value` double NOT NULL DEFAULT '0' COMMENT 'BC cukai',
  `ppn_persen` double NOT NULL DEFAULT '0' COMMENT 'BC ppnper',
  `ppn_value` double NOT NULL DEFAULT '0' COMMENT 'BC ppn',
  `ppnBM_persen` double NOT NULL DEFAULT '0' COMMENT 'BC ppnbmper',
  `ppnBM_value` double NOT NULL DEFAULT '0' COMMENT 'BC ppnbm',
  `pph_persen` double NOT NULL DEFAULT '0' COMMENT 'BC pphper',
  `pph_value` double NOT NULL DEFAULT '0' COMMENT 'BC pph',
  `TotalTax` double NOT NULL DEFAULT '0' COMMENT 'BC total',
  `payment_by` varchar(2) DEFAULT '',
  `PaymentNo` varchar(15) DEFAULT '' COMMENT 'BC bayar',
  `jaminan_by` varchar(2) DEFAULT '',
  `JaminNo` varchar(20) DEFAULT '' COMMENT 'BC jaminan',
  `pejabat` varchar(25) DEFAULT '',
  `nip_pejabat` varchar(15) DEFAULT '',
  `lokasiID` varchar(6) DEFAULT '',
  `ahlikepabeanan` varchar(20) DEFAULT '',
  `flagMinFOB` int(1) NOT NULL DEFAULT '0',
  `flagConnote` int(1) NOT NULL DEFAULT '0',
  `flag_proses` char(1) NOT NULL DEFAULT '0',
  `flag_pecahpos` int(1) NOT NULL DEFAULT '0',
  `GroupID` varchar(20) DEFAULT '',
  `flag_submit` tinyint(1) NOT NULL DEFAULT '0',
  `flag_manual` tinyint(1) NOT NULL DEFAULT '0',
  `flag_Invoice` tinyint(1) NOT NULL DEFAULT '0',
  `flag_POD` tinyint(1) NOT NULL DEFAULT '0',
  `flag_doc_pod` tinyint(1) NOT NULL DEFAULT '0',
  `tgl_doc_pod` varchar(10) DEFAULT NULL,
  `FinanceNo` varchar(10) DEFAULT '',
  `BillingCode` char(3) DEFAULT NULL,
  `Billing_account` varchar(9) DEFAULT NULL,
  `void` int(1) NOT NULL DEFAULT '0',
  `in_out` enum('I','O','') NOT NULL DEFAULT 'I',
  `flag_misroute` char(1) NOT NULL DEFAULT 'F',
  `type_clearance` int(2) NOT NULL DEFAULT '0',
  `type_payment_cust` char(1) NOT NULL DEFAULT 'C',
  `flag_whs_charges` char(1) NOT NULL DEFAULT 'N',
  `flag_api` int(1) NOT NULL DEFAULT '0',
  `api_no` varchar(20) DEFAULT '' COMMENT 'nomor api',
  `flag_lartas` tinyint(1) NOT NULL DEFAULT '0',
  `flag_label` tinyint(1) NOT NULL DEFAULT '0',
  `flag_multi_HS` tinyint(1) NOT NULL DEFAULT '0',
  `pic_location` varchar(50) DEFAULT NULL,
  `updateBY` varchar(50) DEFAULT NULL,
  `updateTime` date DEFAULT NULL,
  `partnercode` varchar(5) NOT NULL DEFAULT '',
  `POD_WH` varchar(15) NOT NULL DEFAULT '',
  `INV_WH` varchar(15) NOT NULL DEFAULT '',
  `cityCodeBYcourier` varchar(3) DEFAULT NULL,
  `CourierName` varchar(3) DEFAULT 'JNT',
  `bigbagNumber` varchar(45) DEFAULT NULL,
  `flag_transfer` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `flag_hold` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `flag_noa` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `flag_proses_bc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TglProses` datetime DEFAULT NULL,
  `TrackingStatus` varchar(5) DEFAULT NULL,
  `TrackingDate` datetime DEFAULT NULL,
  `TrackingRemark` text,
  `SkepKawasan` varchar(50) DEFAULT NULL,
  `Nik` varchar(50) DEFAULT NULL,
  `Id_aju` int(10) DEFAULT NULL,
  `Id_default` int(10) DEFAULT NULL,
  `token` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`noid`),
  KEY `hawb` (`hawb`),
  KEY `type_clearance` (`type_clearance`),
  KEY `TrackingStatus` (`TrackingStatus`),
  KEY `flight_date` (`flight_date`)
) ENGINE=InnoDB AUTO_INCREMENT=98678 DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `telp` varchar(200) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
