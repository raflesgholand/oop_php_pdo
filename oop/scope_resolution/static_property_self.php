<?php
/* Jika kita menggunakan variabel $this untuk mengakses property dan method ‘normal’ dari dalam class, 
maka untuk mengakses static property dan static method,
kita menggunakan keyword “self::”. Berikut contoh penggunaannya:
Pada kode program PHP diatas, saya menggunakan perintah self::$harga_beli, untuk memanggil static property dari dalam class laptop itu sendiri.
 */

// buat class laptop
class laptop {
   public $merk;
   public $pemilik;
   
   // static property
   public static $harga_beli;
  
   //static method
   public static function beli_laptop() {
     return "Beli laptop seharga Rp".self::$harga_beli;
   }
}
  
// set static property
laptop::$harga_beli=4000000;
  
// panggil static method
echo laptop::beli_laptop();
?>