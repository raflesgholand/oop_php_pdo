<?php 

class Connection {
    private $koneksi;
    private $connect;

    public function __construct(){
        $this->koneksi = require_once(__DIR__ . '/database.php');
    }

    public function getConnect(){
        $dsn = $this->koneksi['mysql']['name'].':'.'dbname='.$this->koneksi['mysql']['dbname'].';'.'host='.$this->koneksi['mysql']['host'];
        //$dsn ="mysql:host=localhost;dbname=rest_ci";
        try{
            $this->connect = new PDO($dsn, $this->koneksi['mysql']['username'], $this->koneksi['mysql']['password']);
            return $this->connect;
            // echo $this->connect->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }catch(PDOException $e){
            echo "connect Failed: ".$e->getMessage();
        }
    }
    public function closeconnect(){
        $this->connect=null;
    }
}

// $cek=new Connection();
// return $cek->getConnect();