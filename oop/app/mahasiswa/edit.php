<?php 
require __DIR__.'/../../models/M_mahasiswa.php';
$mhs = new M_mahasiswa;

$mahasiswa = $mhs->getById($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Create mahasiswa</title>
</head>
<body>
    <div class="container p-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form action="update.php" class="form" method="post">
                            <input type="hidden" name="nim" value="<?php echo $mahasiswa->nim; ?>">
                            <div class="form-group">
                                <label for="nama">Name</label>
                                <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $mahasiswa->nama; ?>">
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label><br>
                                <label class="form-check-label">
                                    <input type="radio" name="jenis_kelamin" <?php echo $mahasiswa->jenis_kelamin == 1 ? 'checked':''; ?> value="1" id="jenis_kelamin" class="form-check-inline"> Pria
                                </label>
                                <label for="genre2">
                                    <input type="radio" name="jenis_kelamin" <?php echo $mahasiswa->jenis_kelamin == 0 ? 'checked':''; ?> value="0" id="jenis_kelamin" class="form-check-inline"> Wanita
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea name="alamat" id="alamat" class="form-control"><?php echo $mahasiswa->alamat; ?></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Save" name="save_mahasiswa" class="btn btn-light">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>