<?php
require_once __DIR__.'/../config/Connection.php';

class Model {
    protected $fields = [];
    protected $table;
    protected $primary_key = 'nim';
    private $pdo;

    public function __construct(){
        $this->pdo = new Connection();
    }

    public function serializeData(array $data){
        $keys = [];
        foreach($data as $key => $value){
            // $keys[':'.$key] = $value;
            $keys[]=$value;
        }
        return $keys;
    }
    public function setval(array $data){
        $keys ='';
        foreach($data as $key => $value){
            $keys.="?,";
        }
        return $keys;
    }

    public function all(){
        return $this->pdo->getConnect()->query("select ".implode(',', $this->fields)." from ".$this->table)->fetchAll(PDO::FETCH_OBJ);
    }

    public function getById($nim){
        return $this->pdo->getConnect()->query("select ".implode(',', $this->fields)." from ".$this->table." where nim = '".$nim."'")->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function insert(array $data){
        array_shift($this->fields);
        $fields = implode(',', $this->fields);
        $parameters = $this->serializeData($data);
        $setval = $this->setval($data);
        $tanda_tanya = rtrim($setval, ',');
        $keys = implode(',', array_keys($data));

        $sql = $this->pdo->getConnect()->prepare("insert into ".$this->table." (".$keys.") values(".$tanda_tanya.")");
        $sql->execute($parameters);
        return true;
    }

    public function update($nim, array $data){
        $statement = '';
        foreach($data as $key => $value){
            $statement .= $key."='".$value."',";
        }
        $statement = rtrim($statement, ',');
        $sql = $this->pdo->getConnect()->prepare("update ".$this->table." set ".$statement." where ".$this->primary_key." = '".$nim."'");
        $sql->execute();
        return true;
    }

    public function delete($nim){
        return $this->pdo->getConnect()->exec("delete from ".$this->table." where ".$this->primary_key." = '".$nim."'");
    }
}