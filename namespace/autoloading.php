<?php

//dengan include file memanggil satu satu nama file class
require "Category.php";
require "Post.php";


$category = new Category;
$post = new Post();
echo "Category : " . $category->getName();


echo ", Title : " .  $post->getTitle();
echo ", Isinya : " .  $post->getBody() . "<br>";


// DENGAN AUTOLOADING
spl_autoload_register(function ($class) {
    require_once __DIR__ . '/' . $class . '.php';
});

$category = new Category;
$post = new Post();
echo "Category : " . $category->getName();

echo ", Title : " .  $post->getTitle();
echo ", Isinya : " .  $post->getBody() . "<br>";
